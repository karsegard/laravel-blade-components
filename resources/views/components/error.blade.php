@error($name)
    <span class="error"></span>
    <p class="error">
        {{ $message }}
    </p>
@enderror
