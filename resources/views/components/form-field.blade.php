
<div class="field">
    <label class="field__label">{!! $label !!}</label>
    <div class="field__field">
        {!! $input ?? ''!!}
        <x-kda-blade::error :name="$name"/>
    </div>
</div>
