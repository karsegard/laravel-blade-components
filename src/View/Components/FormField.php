<?php

namespace KDA\BladeComponents\View\Components;

use Illuminate\View\Component;

class FormField extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $label;
    public $input;
    public $name;
    public $edited;
    public function __construct($name="",$label="",$input="",$edited=NULL)
    {
        //
        $this->label = $label;
        $this->input = $input;
        $this->name = $name;
        $this->edited= $edited;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view(config('kda.components.views.form-field'));

    }
}
