<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithViews;
use Illuminate\Support\Facades\Artisan;
use KDA\Tests\TestCase;
use DB;
use KDA\Tests\Models\Post;

class ErrorComponentTest extends TestCase
{
  use InteractsWithViews;

  

  /** @test */
  public function it_renders_the_message_component()
  {
    $view = 
      $this->withViewErrors(['hello'=>'wrong'])
      ->blade(
      '<x-kda-blade::error name="hello"/>',
      []
    );

   
    //$view->assertSeeInOrder(['The message title', 'The message content']);
    $view->assertSee('class="error"',false);
  }
}
