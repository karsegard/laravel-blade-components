<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\Concerns\InteractsWithViews;
use Illuminate\Support\Facades\Artisan;
use KDA\Tests\TestCase;
use DB;
use KDA\Tests\Models\Post;

class FormFieldComponentTest extends TestCase
{
  use InteractsWithViews;

  

  /** @test */
  public function it_renders_the_field_component()
  {
    $view = 
      $this->withViewErrors(['hello'=>'wrong'])
      ->blade(
      '<x-kda-blade::form-field label="Nom" name="lastname">
          
      </x-kda-blade::form-field>
',
      []
    );

    //$view->assertSeeInOrder(['The message title', 'The message content']);
    $view->assertSee('class="field"',false);
  }


  /** @test */
  public function it_renders_the_field_component_label()
  {
    $view = 
      $this->withViewErrors(['hello'=>'wrong'])
      ->blade(
      '<x-kda-blade::form-field label="Nom" name="lastname">
          
      </x-kda-blade::form-field>
',
      []
    );

    //$view->assertSeeInOrder(['The message title', 'The message content']);
    $view->assertSee('Nom',false);
  }


  /** @test */
  public function it_renders_the_field_component_input()
  {
    $view = 
      $this->withViewErrors(['hello'=>'wrong'])
      ->blade(
      '<x-kda-blade::form-field label="Nom" name="lastname">
          <x-slot name="input">
            {{$component->attributes["name"]}}
          </x-slot>
      </x-kda-blade::form-field>',
      []
    );

    //$view->assertSeeInOrder(['The message title', 'The message content']);
    $view->assertSee('Nom',false);
    $view->assertSee('lastname',false);
  }
}
